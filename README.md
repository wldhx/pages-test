# Сайт через `git`

(GitLab Pages, в частности.)

## Как это готовить

1. Fork
2. `git clone $ваш_форк`
3. Сделать изменения
4. git add / commit / push
5. Settings -> Pages -> *клик* на ссылку
6. PROFIT!

## Что читать

- https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/HTML_basics
- https://developer.mozilla.org/en-US/docs/Web/HTML/Element
- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout
